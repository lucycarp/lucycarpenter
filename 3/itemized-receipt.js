// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price
const logReceipt = function() {
  const args = Array.from(arguments);
  console.log(args);
  args.forEach(function(item) {
    description = item.descr;
    amt = item.price;
    console.log(`${description} - $${amt}`)
  } );
};


// Check
logReceipt(
  {descr: 'Burrito', price: 5.99},
  {descr: 'Chips & Salsa', price: 2.99},
  {descr: 'Sprite', price: 1.99}
);
// should log something like:
// Burrito - $5.99
// Chips & Salsa - $2.99
// Sprite - $1.99
// Total - $10.97


// Extra credit
const logReceiptTax = function() {
  const args = Array.from(arguments);
  // remove tax amount from array
  taxAmtPerc = args.shift();
  // show all items and price and add price to subtotal variable
  args.forEach(function(item) {
    const description = item.descr;
    const amt = item.price;
    console.log(`${description} - $${amt}`);
  } );

  // create subtotal amount
  let subTotal = 0;
  args.forEach(function(item) {
    subTotal += Number(item.price);
  } );
  console.log(`Subtotal - $${subTotal}`);

  //Display tax amount
  let taxAmt = 0;
  args.forEach(function(item) {
    taxAmt += Number(item.price * taxAmtPerc);
  } );
  taxAmt = taxAmt.toFixed(2);
  console.log(`Tax - $${taxAmt}`);

  // Display total amount
  taxAmt = Number(taxAmt);
  subTotal = Number(subTotal);
  const totalAmt = taxAmt + subTotal;
  console.log(`Total - $${totalAmt}`);
};

const bud = {descr: 'Bud Light', price: 3.99};
const burger = {descr: 'Hamburger', price: 6.99};
logReceiptTax(0.1, bud, burger);

