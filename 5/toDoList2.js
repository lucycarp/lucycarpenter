// If an li element is clicked, toggle the class "done" on the <li>
const changeToDone = function(){
  const doneMark = this.getElementsByTagName('span')[0];
  doneMark.className = 'done';
}

const liItems = document.querySelectorAll('li');
for (let i = 0; i < liItems.length; i++) {
  liItems[i].addEventListener('click', changeToDone);
}

// If a delete link is clicked, delete the li element / remove from the DOM
const deleteItem = function() {
  // I can't figure out how to delete the specific li...I tried 'this.getElements...' but got errors
  const liItemNode = document.getElementsByTagName('li')[0];
  const liContainer = liItemNode.parentNode;
  liContainer.removeChild(liItemNode);
}

const deleteBtnTrigger = document.getElementsByClassName('delete');
for (let i = 0; i < deleteBtnTrigger.length; i++) {
  deleteBtnTrigger[i].addEventListener('click', deleteItem);
}


// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>
const moveToLater = function() {
  // Get text from list item
  let innerText = document.getElementsByTagName('span')[0].innerHTML;
  console.log(innerText);

  const newLaterNode = document.createElement('li');
  const newLaterSpan = document.createElement('span');
  newLaterSpan.className = 'done';

  let newLiText = document.createTextNode(innerText);
  newLaterSpan.appendChild(newLiText);

  const newTodayBtn = document.createElement('a');
  newTodayBtn.className = 'move toToday';
  const newTodayBtnText = document.createTextNode('Move to Today');
  newTodayBtn.appendChild(newTodayBtnText);
  newTodayBtn.addEventListener('click', moveToToday);

  const newDeleteBtn = document.createElement('a');
  newDeleteBtn.className = 'delete';
  const newDeleteText = document.createTextNode('Delete');
  newDeleteBtn.appendChild(newDeleteText);
  newDeleteBtn.addEventListener('click', deleteItem);
  
  newLaterNode.appendChild(newLaterSpan);
  newLaterNode.appendChild(newTodayBtn);
  newLaterNode.appendChild(newDeleteBtn);

  const positionLater = document.getElementsByClassName('later-list')[0];
  positionLater.appendChild(newLaterNode);

  changeToDone();
  deleteItem();
}
const moveToToday = function() {
  // Get text from list item
  let innerText = document.getElementsByTagName('span')[0].innerHTML;
  console.log(innerText);

  const newTodayNode = document.createElement('li');
  const newTodaySpan = document.createElement('span');
  newTodaySpan.className = 'done';

  let newLiText = document.createTextNode(innerText);
  newTodaySpan.appendChild(newLiText);

  const newLaterBtn = document.createElement('a');
  newLaterBtn.className = 'move toLater';
  const newLaterBtnText = document.createTextNode('Move to Later');
  newLaterBtn.appendChild(newLaterBtnText);
  newLaterBtn.addEventListener('click', moveToLater);

  const newDeleteBtn = document.createElement('a');
  newDeleteBtn.className = 'delete';
  const newDeleteText = document.createTextNode('Delete');
  newDeleteBtn.appendChild(newDeleteText);
  newDeleteBtn.addEventListener('click', deleteItem);
  
  newTodayNode.appendChild(newTodaySpan);
  newTodayNode.appendChild(newLaterBtn);
  newTodayNode.appendChild(newDeleteBtn);

  const positionLater = document.getElementsByClassName('today-list')[0];
  positionLater.appendChild(newTodayNode);
  
  changeToDone();
  deleteItem();
}
const moveToLaterBtn = document.getElementsByClassName('move toLater');
for (let i = 0; i < moveToLaterBtn.length; i++) {
  moveToLaterBtn[i].addEventListener('click', moveToLater);
}
const moveToTodayBtn = document.getElementsByClassName('move toToday');
for (let i = 0; i < moveToTodayBtn.length; i++) {
  moveToTodayBtn[i].addEventListener('click', moveToToday);
}


// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!  
// Make sure to add an event listener to your new <li> (if needed)
const addListItem = function(e) {
  e.preventDefault();
  const input = this.parentNode.getElementsByTagName('input')[0];
  const text = input.value; // use this text to create a new <li>
  
  let newTodayNode = document.createElement('li');

  let newSpanToday = document.createElement('span');
  let newText = document.createTextNode(text);
  newSpanToday.appendChild(newText);

  let newLaterBtn = document.createElement('a');
  newLaterBtn.className = 'move toLater';
  let newLaterBtnText = document.createTextNode('Move to Later');
  newLaterBtn.appendChild(newLaterBtnText);
  newLaterBtn.addEventListener('click', moveToLater);

  let newDeleteBtn = document.createElement('a');
  newDeleteBtn.className = 'delete';
  let newDeleteText = document.createTextNode('Delete');
  newDeleteBtn.appendChild(newDeleteText);
  newDeleteBtn.addEventListener('click', deleteItem);
  
  newTodayNode.appendChild(newSpanToday);
  newTodayNode.appendChild(newLaterBtn);
  newTodayNode.appendChild(newDeleteBtn);

  let positionLater = document.getElementsByClassName('today-list')[0];
  positionLater.appendChild(newTodayNode);
}

const addListItemLater = function(e) {
  e.preventDefault();
  const input = this.parentNode.getElementsByTagName('input')[0];
  const text = input.value; // use this text to create a new <li>
  
  let newTodayNode = document.createElement('li');

  let newSpanToday = document.createElement('span');
  let newText = document.createTextNode(text);
  newSpanToday.appendChild(newText);

  let newLaterBtn = document.createElement('a');
  newLaterBtn.className = 'move toLater';
  let newLaterBtnText = document.createTextNode('Move to Later');
  newLaterBtn.appendChild(newLaterBtnText);
  newLaterBtn.addEventListener('click', moveToLater);

  let newDeleteBtn = document.createElement('a');
  newDeleteBtn.className = 'delete';
  let newDeleteText = document.createTextNode('Delete');
  newDeleteBtn.appendChild(newDeleteText);
  newDeleteBtn.addEventListener('click', deleteItem);
  
  newTodayNode.appendChild(newSpanToday);
  newTodayNode.appendChild(newLaterBtn);
  newTodayNode.appendChild(newDeleteBtn);

  let positionLater = document.getElementsByClassName('later-list')[0];
  positionLater.appendChild(newTodayNode);
}


// Add this as a listener to the two Add links
const addBtnToday = document.getElementsByClassName('add-item')[0];
addBtnToday.className = 'add-item-today';
addBtnToday.addEventListener('click', addListItem);
const addBtnLater = document.getElementsByClassName('add-item')[0];
addBtnLater.addEventListener('click', addListItemLater);

//1 point: Show a checkmark in front of completed task descriptions (checkmark should be removed if task is clicked again)
const removeCheckmark = function(){
  let innerText = this.getElementsByTagName('span')[0];
  innerText.innerHTML = innerText.innerHTML.substring(2);
  // couldn't figure out how to apply this to each individual li
  liItemsClick[0].removeEventListener('click', removeCheckmark);
  liItemsClick[0].addEventListener('click', Checkmark);
}

const checkmark = function(){
  let innerText = this.getElementsByTagName('span')[0];
  innerText.innerHTML = `&#x2713 ${innerText.innerHTML}`;
  // couldn't figure out how to apply this to each individual li
  liItemsClick[0].removeEventListener('click', checkmark);
  liItemsClick[0].addEventListener('click', removeCheckmark);
}

const liItemsClick = document.querySelectorAll('li');
for (let i = 0; i < liItemsClick.length; i++) {
  liItems[i].addEventListener('click', checkmark);
}

