// I ran these answers in the console to get the results

// 1 
Math.round(Math.random() * 13);

// 2
var card1 = Math.ceil(Math.random() * 13);
var card2 = Math.ceil(Math.random() * 13);
var card3 = Math.ceil(Math.random() * 13);

var highCard = Math.max(card1, card2, card3);

// 3
// Make variables based on inches and then variables to represent square inches 
var smallPizzaSize = 13;
var largePizzaSize =  17;

var smallPizzaSqIn = Math.PI * ((smallPizzaSize/2)**2);
var largePizzaSqIn = Math.PI * ((largePizzaSize/2)**2);



// 4
var smallSqInCost = 16.99 / smallPizzaSqIn;
var largeSqInCost = 19.99 / largePizzaSqIn;

var small = smallSqInCost.toFixed(2);
// 0.13
var large = largeSqInCost.toFixed(2);
// 0.09


