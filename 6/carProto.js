/**
 * Creates a new Car object
 * @constructor
 * @param {String} model 
 */
const Car = function(model) {
  this.model = model;
  this.currentSpeed = 0;
}
Car.prototype.accelerate = function() {
  this.currentSpeed += 1;
}
Car.prototype.brake = function() {
  this.currentSpeed -= 1;
}
Car.prototype.toString = function() {
  return `The ${this.model} is going ${this.currentSpeed}`
}
