const assignmentDate = '1/21/2019';

// Convert to a Date instance
let startDate = new Date(assignmentDate);

// Create dueDate which is 7 days after assignmentDate
let dueDate = new Date(startDate.getTime() + 7 * 24 * 60 * 60 * 1000);
console.log(dueDate); 

// Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// log this value using console.log
let options = { year: 'numeric', month: 'long', day: 'numeric' };
let dueDateFormat = new Intl.DateTimeFormat('en-US', options).format(dueDate);
console.log(dueDateFormat);

let tagTime = document.getElementsByTagName('time');
tagTime[0].innerHTML = dueDateFormat;


// Option 2: return it as an HTML string 
let dueDateYear = dueDate.getFullYear();
let dueDateMonthNumerical = dueDate.getMonth() + 1;
let dueDateDayNumerical = dueDate.getDate();
let dueDateFormat2 = `<time datetime="${dueDateYear}-${dueDateMonthNumerical}-${dueDateDayNumerical}">${dueDateFormat}</time>`;
console.log(dueDateFormat2);