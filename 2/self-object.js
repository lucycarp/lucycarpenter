const lucy = {
  firstName: 'Lucy',
  lastName: 'Carpenter',
  'favorite food': 'chocolate',
  mom: {
    firstName: 'JoAnn',
    lastName: 'Slattery',
    'favorite food': 'oranges'
  },
  dad: {
    firstName: 'Tom',
    lastName: 'Carpenter',
    'favorite food': 'mushrooms',
  },
}

console.log(lucy.dad.firstName);
console.log(lucy.mom['favorite food']);

