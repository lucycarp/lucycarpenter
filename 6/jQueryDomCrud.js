$( document ).ready(function() {
// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
let $newA = $('<a>');
$newA.text('Buy Now!');
$newA.attr('href', '');
$newA.addClass('cta');

$('main').append($newA);


// Access (read) the data-color attribute of the <img>,
// log to the console
const $img = $('img');
console.log($img.attr('data-color'));


// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"
const $li = $('li');
const $thirdItem = $($li[2]);
$thirdItem.addClass('highlight');

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
const $p = $('p');
$p.remove();


});