// Attach one listener that will detect clicks on any of the <td>
// elements.  Should update that element's innerHTML to be the
// x, y coordinates of the mouse at the time of the click


const tdItems = document.getElementsByTagName('td');
for (let i = 0; i < tdItems.length; i++) {
  tdItems[i].addEventListener('click', function(e){
    let tdItem = e.target;
    this.innerHTML = `${e.clientX}, ${e.clientY}`;
  });
}