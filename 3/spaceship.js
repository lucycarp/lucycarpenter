
const SpaceShip = function SpaceShip(shipName,shipTopSpeed) {
  const name = shipName;
  let topSpeed = shipTopSpeed;
  this.accelerate = function() {
    console.log(`${name} moving to ${topSpeed} mph!`);
  };
  this.speedChange = function(newTopSpeed) {
    topSpeed = newTopSpeed;
  };
};

const blueShip = new SpaceShip('Blue Phaser', '200');
blueShip.accelerate();

// change top speed
blueShip.speedChange(300);
blueShip.accelerate();