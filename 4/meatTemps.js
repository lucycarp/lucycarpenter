/**
 * Determines whether meat temperature is high enough
 * @param {string} kind 
 * @param {number} internalTemp 
 * @param {string} doneness
 * @returns {boolean} isCooked
 */
const foodIsCooked = function(kind, internalTemp, doneness) {
  if (kind === 'chicken') {
    if (internalTemp >= 165) {
      foodIsCooked.doneness = true;
      return foodIsCooked.doneness;
    } else {
      foodIsCooked.doneness = false;
      return foodIsCooked.doneness;
    }
  } else if (kind === 'beef') {
    if (internalTemp >= 155 && doneness === 'well') {
      foodIsCooked.doneness = true;
      return foodIsCooked.doneness;
    } else if ((internalTemp >= 135 && internalTemp < 155) && doneness === 'medium'){
      foodIsCooked.doneness = true;
      return foodIsCooked.doneness;
    } else if ((internalTemp >= 125 && internalTemp < 135) && doneness === 'medium'){
    foodIsCooked.doneness = true;
    return foodIsCooked.doneness;
    } else {
    foodIsCooked.doneness = false;
    return foodIsCooked.doneness;
    }
  } else {
    console.log('Please check that you have stated the type of meat and internal temp');
    return false;
  }
}



// Test function
console.log(foodIsCooked('chicken', 90)); // should be false
console.log(foodIsCooked('chicken', 190)); // should be true
console.log(foodIsCooked('beef', 138, 'well')); // should be false
console.log(foodIsCooked('beef', 138, 'medium')); // should be true
console.log(foodIsCooked('beef', 128, 'rare')); // should be true