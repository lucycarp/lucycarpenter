// I ran these answers in the console to get the results

//Start with dates
const endDate = new Date(2019,3,1);
const startDate = new Date(2019,0,1);

//Find milliseconds from jan 1, 1970 to each date
const endDateMilSec = endDate.getTime();
const startDateMilSec = startDate.getTime();

//Minus startDate from EndDate milliseconds to get milliseconds to middle date
var middleDateMilSec = (endDateMilSec - startDateMilSec) / 2;

//Minus the milliseconds to the middle from the end date to find the middle date
var endDateMinusMiddle = endDateMilSec - middleDateMilSec;
var middleDate = new Date(endDateMinusMiddle);

// result: 2019-02-15T07:30:00.000Z