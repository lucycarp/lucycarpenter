$( document).ready(function() {

// If an li element is clicked, toggle the class "done" on the <li>
const $li = $('li');
$li.on('click', function(e){
  let $this = $(this);
  $this.toggleClass('done');
})

// If a delete link is clicked, delete the li element / remove from the DOM
const $deleteBtn = $('.delete');
$deleteBtn.on('click', function(e){
  let $this = $(this);
  $this.parent().fadeOut(1000);
  e.stopPropagation();
})


// If a "Move to..." link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>
const moveToToday = function(e){
  let $this = $(this);
  $( ".today-list" ).append($this.parent());
  $this.parent().toggleClass('done');
  $this.text('Move To Later');
  $this.off('click');
  $this.on('click', moveToLater);
}

const moveToLater = function(e){
  let $this = $(this);
  $( ".later-list" ).append($this.parent());
  $this.parent().toggleClass('done');
  $this.text('Move To Today');
  $this.off('click');
  $this.on('click', moveToToday);
}

const $moveToLaterBtn = $('.movetoLater');
$moveToLaterBtn.on('click', moveToLater);

const $moveToTodayBtn = $('.move');
$moveToTodayBtn.on('click', moveToToday);


// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!  
// Make sure to add an event listener to your new <li> (if needed)
const addListItemToday = function(e) {
  e.preventDefault();
  const text = $(this).parent().find('input').val();
  
  let $newLi = $('<li>');
  let $newSpan = $('<span>');
  $newSpan.text(text);
  let $newDelete = $('<a>');
  let $newMoveLater = $('<a>');
  $newMoveLater.text('Move To Later');
  $newMoveLater.on('click', moveToLater);
  $newMoveLater.addClass('movetoLater');
  $newDelete.text('Delete');
  $newDelete.addClass('delete');
  $newDelete.on('click', function(e){
    let $this = $(this);
    $this.parent().remove();
  });

  $newLi.append($newSpan);
  $newLi.append($newMoveLater);
  $newLi.append($newDelete);

  let $todayListUl = $('.today-list');
  $todayListUl.append($newLi);
}

const addListItemLater = function(e) {
  e.preventDefault();
  const text = $(this).parent().find('input').val();
  
  let $newLi = $('<li>');
  let $newSpan = $('<span>');
  $newSpan.text(text);
  let $newDelete = $('<a>');
  let $newMoveToday = $('<a>');
  $newMoveToday.text('Move To Today');
  $newMoveToday.on('click', moveToToday);
  $newMoveToday.addClass('move');
  $newDelete.text('Delete');
  $newDelete.addClass('delete');

  $newDelete.on('click', function(e){
    let $this = $(this);
    $this.parent().remove();
  });

  $newLi.append($newSpan);
  $newLi.append($newMoveToday);
  $newLi.append($newDelete);

  let $todayListUl = $('.later-list');
  $todayListUl.append($newLi);
  

}



// Add this as a listener to the two Add links
const $addBtnToday = $('.add-item-today');
$addBtnToday.on('click', addListItemToday);

const $addBtn = $('.add-item');
$addBtn.on('click', addListItemLater);

});