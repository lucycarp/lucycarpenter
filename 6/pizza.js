/**
 * Creates a new instance of Pizza
 * @param {String} kind 
 */

const Pizza = function(kind) {
    this.kind = kind;
    this.slices = 8; 
 }
Pizza.prototype.eatSlice = function() {
   this.slices -= 1;
 }
Pizza.prototype.toString = function() {
  return `The ${this.kind} pizza has ${this.slices} slices left!`
}

const mushroomPizza = new Pizza('mushroom');
mushroomPizza.eatSlice();
console.log(mushroomPizza.toString());

const cheesePizza = new Pizza('cheese');
cheesePizza.eatSlice();
cheesePizza.eatSlice();
cheesePizza.eatSlice();
cheesePizza.eatSlice();
cheesePizza.eatSlice();
console.log(cheesePizza.toString());