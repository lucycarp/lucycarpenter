// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
let buyNowLinkText;
let buyNowLinkNode;
let main;
// create new element node
buyNowLinkNode = document.createElement('a');
// create new text node
buyNowLinkText = document.createTextNode('Buy Now!');
// Set attributes
buyNowLinkNode.setAttribute('href', '');
buyNowLinkNode.setAttribute('id', 'cta');
// Link the new text node to the a element
buyNowLinkNode.appendChild(buyNowLinkText);
// Find the parent node where it should be added to 
main = document.getElementsByTagName('main')[0];
// Add buyNowLink to html markup
main.appendChild(buyNowLinkNode);

// Access (read) the data-color attribute of the <img>,
// log to the console
const carImg = document.getElementsByTagName('img')[0];
const carImgColor = carImg.dataset.color;
console.log(carImgColor);


// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"
const list = document.getElementsByTagName('ul')[0];
const listItems = list.children;
const thirdListItem = listItems[2];
thirdListItem.className = 'highlight';


// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
const purchaseParagraph = document.getElementsByTagName('p')[0];
const containerParagraph = purchaseParagraph.parentNode;
containerParagraph.removeChild(purchaseParagraph);
