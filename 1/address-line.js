// I ran these answers in the console to get the results

// 1 
var firstName = 'Samantha';
var lastName = 'Smith';
var streetAddress = '100 Sesame Steeet';
var city = 'New York';
var state = 'New York';
var zipCode = '10093';
var cityState = city + ', ' + state; 

var printAddress = `${firstName} ${lastName}\n${streetAddress}\n${cityState}\n${zipCode}`

// 2
var user1 = 'Samantha Smith\n100SesameStreet\nNew York, New York\n10093';

var firstLine = user1.indexOf(0,'\n');
// result: 16 - so get the right letters, I need to subtract the 1 '\n' space
var firstLineText = user1.slice(0,15);

var secondLine = user1.indexOf('\n','\n');
// result: 14 - this means that the first line is 14 characters long, so the second line must start after the \n at 16
var secondLineText = user1.slice(16,30);

//I know the zip code is always 5 characters so take that from the end
var zipCodeLineText = user1.slice(-5);

//Since the zip code plus the \n characters equals 6, I can use that as the last number
var thirdLineText = user1.slice(31,-6);







